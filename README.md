# guifi-tools-and-scripts

- flash-tool-tftp: Script that facilitates flashing ubiquiti devices for qMp or other firmwares, specially in workshops
- dynGRE: Script to use GRE tunnels with dynamics IPs
- sentilo docker image to perform a fast development deploy
- [in development] guifi docker image to facilitate web development
- ansible for qmp: an admin tool to simply management of community networks
