"""Mikrotik HTTP Handler for alert xat guifi

Mikrotik sucks: cannot curl, cannot use ssh to send data. Fetch is the way to go. That's why this stuff is required.
Inspired on SimpleHTTPServer

You probably found this here: https://github.com/pedro-nonfree/guifi-tools-and-scripts

"""

__version__ = "0.1"

__all__ = ["MikrotikHTTPHandler"]

import BaseHTTPServer
#urllib is an alternative to usage of curl src: http://stackoverflow.com/questions/26000336/execute-curl-command-within-a-python-script
#import urllib
# parse url parameters
import urlparse
# verify if bash script is there
import os.path
# call bash script
import subprocess

# global variables to change appropriatelly
# script that calls through curl RocketChat API
script_name = "./alert-xat-guifi.sh"
# change appropriatelly
secret = "mysecret"

class SimpleHTTPRequestHandler(BaseHTTPServer.BaseHTTPRequestHandler):

    """Simple HTTP request handler with GET and HEAD commands.

    This parses url looking for query parameters and adapt 
    them to send a curl command (in another bash script) to
    RocketChat API. Data parameter sends information. Secret
    parameter will help us to authorize devices/folks.

    The GET and HEAD requests are identical except that the HEAD
    request omits the actual contents of the file.

    """

    server_version = "MikrotikHTTPHandler/" + __version__

    def do_GET(self):
        """Serve a GET request."""
        f = self.send_head()
        if f:
            f.close()

    def do_HEAD(self):
        """Serve a HEAD request."""
        f = self.send_head()
        if f:
            f.close()

    def send_head(self):
        """Common code for GET and HEAD commands.

        Here be dragons

        """

        # src http://stackoverflow.com/a/5075477
        url = self.path
        parsed = urlparse.urlparse(url)

        query = parsed.query
        if query:
            # remove last character '/' in query
            if query[-1] == '/':
                query = query[:-1]
            try:
               data = ''.join ( urlparse.parse_qs(query)['data'] )
               secret_try = ''.join ( urlparse.parse_qs(query)['secret'] )
            except:
                self.send_error(400, "Bad Request")
                return None
            if secret_try != secret:
                self.send_error(401, "Unauthorized")
                return None

            # src http://stackoverflow.com/questions/13745648/running-bash-script-from-within-python
            subprocess.call( script_name + " " + data, shell=True)

        self.send_response(200)

def test(HandlerClass = SimpleHTTPRequestHandler,
         ServerClass = BaseHTTPServer.HTTPServer):
    BaseHTTPServer.test(HandlerClass, ServerClass)


if __name__ == '__main__':

    if os.path.isfile( script_name ):
        test()
    else:
        print script_name + " not found"
