#!/bin/bash

# version 0.1
# based on: https://rocket.chat/docs/developer-guides/rest-api/

user="myuser"
password="mypassword"
url="https://xat.guifi.net"
message="$@"

logon="$(curl -s $url/api/login \
        -d "user=$user&password=$password")"

# HARDCODED API parsing
authToken=$(echo $logon | cut -d: -f4 | cut -d, -f1 | sed s/\"//g )
userId=$(echo $logon | cut -d: -f5 | sed s/[\"}]//g )

# get ID of public room
#curl -H "X-Auth-Token: $authToken" \
#     -H "X-User-Id: $userId" \
#        $url/api/publicRooms

# HARDCODED roomId for https://xat.guifi.net/channel/alertes
roomId="CpAroRmd4c3aB3PKj"

# send msg
curl -s -H "X-Auth-Token: $authToken" \
     -H "Content-Type: application/json" \
     -X POST \
     -H "X-User-Id: $userId" \
        $url/api/rooms/$roomId/send \
     -d "{ \"msg\" : \"$message\" }" &> /dev/null

# logoff
curl -s -H "X-Auth-Token: $authToken" \
     -H "X-User-Id: $userId" \
     $url/api/logout &> /dev/null
