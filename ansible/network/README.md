# What's that?

Automating administrative tasks (ansible playbooks) for network purposes.

Assuming that the embedded devices do not have python due to size limitation, `raw` and `script` modules are the options available to do remote tasks in the nodes. Additionally there is the`local_action` to use all python modules from the management host.

# Requirements

For the community network devices just a requirement: ssh connection available.

For the management host:
- Python 2.7 available
- Install ansible with pip. Commands: `easy_install pip` and `pip install -U ansible`
- Use or create ssh public key. Create: `ssh-keygen -t rsa -b 4096 -C me@here -f ~/.ssh/<my_id>`. If you do not set password to key, you will never ask for a password.
- Before using public key you have to pass a passphrase. Install: `apt-get install sshpass` 
- Avoid checking ssh fingerprints. Put in your ~/.ssh/config:
```
# Avoid Checkings
Host 10.*
    StrictHostKeyChecking no
    UserKnownHostsFile=/dev/null
```
Alternatively, If you care for security and you have time, retrieve all your node's ssh fingerprints and put them in ~/.ssh/known_hosts:
- Use scp instead of sftp. Create ~/.ansible.cfg and put:
```
[ssh_connection]

# if True, make ansible use scp if the connection type is ssh 
# (default is sftp)
scp_if_ssh = True
```

# What can I do with this?

First of all, put your ssh public key in all nodes you want to manage. Use [addkey](addkey/) ansible playbook to do that.

After that you can execute different playbooks to a set of nodes:
- Generic
  - Send a specific command directly [cmd](cmd)
  - Upload different files and/or directories [ul](ul)
  - *Coming soon!* Download different files and/or directories [dl](dl)
  - Run the same command at the same time [swarmsync](swarmsync). Refactor required
- qMp
  - Perform qmp firmware upgrade with internet connection [qmpup](qmpup)
  - *Coming soon!* Perform qmp firmware upgrade without internet connection [qmpup](qmpup)
  - *Coming soon!* Set coordinates to show nodes [libremap](http://libremap.net) [qmpcoords](qmpcoords)
  - *Coming soon!* Download backups [sysbkp](sysbkp)

To know what does a particular playbook start reading its `.yml` file, and if you have more doubts continue with its scripts.

To execute them, for example, you can use: `ansible-playbook -i inventory_file -u root playbook/playbook.yml`

Based on that, you can send commands and scripts to the managed nodes. Pull requests are welcome :-)

## Ideas / Challenges / TODO

- Change wifi channel

# Inventory file

Each ansible playbook requires a list of hosts, an inventory file. You can use one that is universal, or adapt one for each task. In case of a critical task, for example a firmware upgrade, the order of the list will be important.

This is how my inventory file looks:

```
nodename_10.1.2.1   ansible_host=10.1.1.1   device_model=NanoStation-M5  nid=43232

node2name_10.1.2.1  ansible_host=10.1.2.1   device_model=Rocket-M5       nid=12341
```

Notes:
- `device_model` variable is required for upgrade purposes
- `nid` is node ID for guifi.net community network nodes
