Put your ssh public key in all nodes you want to manage.

## Instruction steps

1. Copy or edit your public key as `key.pub`.
2. By default `inventory_file` is taken from parent directory. Change this behavior editing `addkey.sh`. See more information about `inventory_file` [here](../README.md).
3. Execute `addkey.sh` to start ansible playbook. Type qmp default password: `13f`.
4. To start using the rest of administrative tasks with ansible add key `ssh-add ~/.ssh/<my_id>`.
  - Troubleshooting: http://stackoverflow.com/questions/17846529/could-not-open-a-connection-to-your-authentication-agent
5. Check access to a host `ssh root@<ip>`, you should login with your key password, or with no password (depends on the generated key).
