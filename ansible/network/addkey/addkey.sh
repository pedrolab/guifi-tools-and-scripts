#!/bin/sh

# src http://stackoverflow.com/q/4774054
# Absolute path to this script.
SCRIPT=$(readlink -f $0)
# Absolute path to its parent
SCRIPTPATH=`dirname $SCRIPT`
cd $SCRIPTPATH

ansible-playbook -i ../inventory_file -u root addkey.yml -k
