#!/bin/sh

key=$1

if grep "$key" /etc/dropbear/authorized_keys &> /dev/null; then
    echo key already present
else
    # functionality of ssh-key-copy for openwrt: append key to authorized keys and verify that is unique
    echo $key >> /etc/dropbear/authorized_keys \
        && sort /etc/dropbear/authorized_keys | uniq > /etc/dropbear/authorized_keys.temp \
        && mv /etc/dropbear/authorized_keys.temp /etc/dropbear/authorized_keys \
        && chmod 0600 /etc/dropbear/authorized_keys \
        && chmod 0700 /etc/dropbear
    echo key changed
fi
