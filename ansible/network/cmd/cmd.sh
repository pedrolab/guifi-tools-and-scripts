#!/bin/sh

# Absolute path to this script.
SCRIPT=$(readlink -f $0)
# Absolute path to its parent
SCRIPTPATH=`dirname $SCRIPT`
cd $SCRIPTPATH

args="$@"
ansible all -i ../inventory_file -u root -c ssh -m raw -a "$args"
