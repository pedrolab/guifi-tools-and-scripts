#!/bin/bash

# Observation: m csv lines per n nodes ssh connections

# ip address
addr="$1"
# filename or string where we get sources and destinations to files and directories
filename="/tmp/dl.csv"

errorlog="error.log"
echo "--------------$(date +'%Y%m%d_%H%M%S')--------------" >> $errorlog

while IFS='' read -r line || [[ -n "$line" ]]; do

    # if string starts with #, ignore that line, is a comment
    if [ "${line:0:1}" = '#' ]; then
        continue
    fi

    # source (first column) and destination (second column) paths
    src="$(echo $line | cut -d',' -f1)"
    dest="$(echo $line | cut -d',' -f2)"

    # if source or destination are empty, or source path does not exist, ignore this line and go to the next one
    if [ -z $src ] || [ -z $dest ] || [ ! -d $dest ]; then
        echo "invalid source and destination. src:$src, dest:$dest" >> $errorlog
        continue
    fi

    # copy
    scp -r root@${addr}:${src} $dest || echo no such remote source $addr:$src >> $errorlog; continue

    # delete previous location (avoid error when overwriting directory)
    rm -rf "$dest/node-$(echo $addr | tr . -)-data-$(basename $src)" 
    # rename uniquely appending of host ip address
    mv -f "$dest/$(basename $src)" "$dest/node-$(echo $addr | tr . -)-data-$(basename $src)" >> $errorlog 2>&1

# src [iterate with file lines] http://stackoverflow.com/a/10929511
done < "$filename"
