#!/bin/bash

# Absolute path to this script.
SCRIPT=$(readlink -f $0)
# Absolute path to its parent
SCRIPTPATH=`dirname $SCRIPT`
cd $SCRIPTPATH

ansible-playbook dl.yml -i ../inventory_file -u root
