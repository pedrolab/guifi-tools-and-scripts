Perform qMp firmware upgrade with internet connection.

## Instructions

- Each host added in `inventory_file` requires to specify `device_model` to use this playbook
- Each `device_model` should have a link address in `upgrade.csv` file
- Select which `target` you want to install in `decision.sh` file
