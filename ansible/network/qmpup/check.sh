#!/bin/sh

# perform different tests to check that the node is working

# This parameter should output more than 3 lines
# this is how it looks the 3 line output:

# originators:
# name                        blocked primaryIp                         routes viaIp                     viaDev     metric lastDesc lastRef 
# mynode-ABCD                 e       f                                 g      ::                        ---        h      i        j

if [ $(bmx6 -c originators | wc -l) -le 3 ]; then
    exit 1
fi 

# check if Internet is working with an example
if ! ping -w1 -c1 wikipedia.org &> /dev/null; then
    exit 1
fi
