#!/bin/sh

# check that it will be possible to download the firmware

if ! ping -w1 -c1 fw.qmp.cat &> /dev/null; then
    echo Device will not download firmware. Fix internet connection.
    exit 1
fi

# compare target version with current version in this device

target="qMp Clearance 3.2, master rev.9782ba8"
current=$(qmpinfo version | sed 's/\"//g' | cut -d- -f1)
if [ "$target" = "$current" ]; then
   echo Firmware already upgraded.
else
   echo Upgrade required
fi
