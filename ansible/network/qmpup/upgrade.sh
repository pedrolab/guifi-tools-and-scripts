#!/bin/sh

# upgrade with appropriate version of qMp depending on the device

device_model=$1
link=$(cat upgrade.csv | grep $device_model, | cut -d',' -f2)

ssh root@$2 "( echo y | qmpcontrol upgrade $link &>/tmp/sysup.log ) & exit"

echo Upgrading
