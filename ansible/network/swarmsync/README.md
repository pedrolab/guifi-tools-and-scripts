Run the same command at the same time.

## Instructions
- The `swarmsync.sh` script contains comments to assist you on what changes you have to introduce to start working on it. There you will have to select: a date notation, and an specific date to perform the script, as well as the action itself you want to run.
