#!/bin/sh

while true; do

    # alternatively, a full date notation
    # YearMonthDay_HourMinuteSecond="20160126_1903"
    #while $(date +"%Y%m%d_%H%M%S")

    HourMinute="1903"
    if [ $(date +"%H%M"  ) = "$HourMinute"]; then

        # here run commands or scripts

        exit # finished
    fi

    sleep 0.5; # time betwen checks, adjust to your necessities

done;
