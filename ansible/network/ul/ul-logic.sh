#!/bin/bash

# Observation: m csv lines per n nodes ssh connections

# ip address
addr="$1"
# filename or string where we get sources and destinations to files and directories
filename="/tmp/ul.csv"

while IFS='' read -r line || [[ -n "$line" ]]; do


    # source (first column) and destination (second column) paths
    src="$(echo $line | cut -d',' -f1)"
    dest="$(echo $line | cut -d',' -f2)"

    # option: with 0 value, backup is disabled
    opt="$(echo $line | cut -d',' -f3)"

    # if source or destination are empty, or source path does not exist, ignore this line and go to the next one
    if [ -z $src ] || [ -z $dest ] || [ ! -e $src ]; then
        continue
    fi

    # src [ssh breaks while statement] http://stackoverflow.com/questions/9393038/ssh-breaks-out-of-while-loop-in-bash
    # src [dates] http://unix.stackexchange.com/questions/96380/how-to-append-date-to-backup-file
    # if file exist perform backup
    # if opt variable is 0, no backup is performed: this action is skipped
    ssh root@${addr} <<EOF
[ -e "$dest" ] && [ "$opt" != "0" ] && cp -r $dest ${dest}.orig-$(date +'%Y%m%d_%H%M%S')-ansible;
EOF

    # copy
    scp -r $src root@${addr}:${dest}

# src [iterate with file lines] http://stackoverflow.com/a/10929511
done < "$filename"
