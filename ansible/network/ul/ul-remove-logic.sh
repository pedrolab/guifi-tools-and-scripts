#!/bin/bash

# Observation: m csv lines per n nodes ssh connections

# ip address
addr="$1"
# filename or string where we get sources and destinations to files and directories
filename="/tmp/ul.csv"

while IFS='' read -r line || [[ -n "$line" ]]; do

    # source (first column) and destination (second column) paths
    src="$(echo $line | cut -d',' -f1)"
    dest="$(echo $line | cut -d',' -f2)"

    # if destination is empty or path is very dangerous, ignore this line and go to the next one
    if [ -z $dest ] || [ "$dest" = "/" ]; then
        continue
    fi

    # clean destination directory of backup files
    ssh root@${addr} <<EOF
cd $(dirname "$dest")
rm -rf "$dest"
EOF

# src [iterate with file lines] http://stackoverflow.com/a/10929511
done < "$filename"
