#!/bin/bash

if [ ! -e /var/www/guifi/sites/default.settings.php ]; then
    # wait mysql init
    #sleep 14

    # wait mysql init
    # src http://stackoverflow.com/questions/21183088/how-can-i-wait-for-a-docker-container-to-be-up-and-running
    while ! curl -s mysql:3306 &> /dev/null
    do
        echo "$(date) - still trying (note: this image will not work without docker compose)"
        sleep 4
    done 
    echo "$(date) - connected successfully"

    drush site-install default -y --account-name=admin --account-pass=admin --db-url=mysql://drupal:drupalp@mysql/drupal

    # report status says directory sites/default/files is not writable
    chown www-data:www-data /var/www/guifi/sites/default/files
    chmod +w /var/www/guifi/sites/default/files

    # Solves this drupal report status error: Multibyte string input conversion in PHP is active and must be disabled. Check the php.ini mbstring.http_input setting. Please refer to the PHP mbstring documentation for more information.
    # src says this is bad practice https://www.drupal.org/node/87138
    chmod +w /var/www/guifi/sites/default/settings.php
    echo "ini_set('mbstring.http_input','pass');" >> /var/www/guifi/sites/default/settings.php
    echo "ini_set('mbstring.http_output','pass');" >> /var/www/guifi/sites/default/settings.php
    echo "\$db_collation = 'utf8_general_ci';" >> /var/www/guifi/sites/default/settings.php
    chmod -w /var/www/guifi/sites/default/settings.php



    # Installation of generic drupal modules/projects requeriments for guifi web
    # issues:
    # potx's status "Installed version not supported"
    #drush dl -y views_slideshow-6.x-2.4 potx-6.x-3.x-dev cck
    drush dl -y views_slideshow-6.x-2.4 cck

    # cck installation
    drush en -y content_permissions text nodereference fieldgroup userreference content_copy number optionwidgets content


    # views_slideshow
    drush en -y views_slideshow views_slideshow_singleframe views_slideshow_thumbnailhover

    # i18n
    # without this, you don't get frontpage when the theme is installed
    drush en -y i18n
    drush en -y i18nmenu i18ntaxonomy i18nprofile i18nmenu i18ncck i18nsync i18ncontent i18nblocks i18nstrings i18npoll

    # image
    drush en -y image
    drush en -y image_gallery image_import image_im_advanced image_attach

    # rest
    # issues:
    # Translation template extractor (potx)  6.x-3.3+66-dev     6.x-3.3+66-dev    Installed version not supported
    # potx \
    drush en -y \
        ckeditor \
        captcha \
        devel \
        diff \
        event \
        fivestar \
        image_filter \
        l10n_client \
        language_sections \
        schema \
        views \
        votingapi \
        webform 

    cd /var/www/guifi/sites/all/modules/ckeditor/ckeditor
    #CKEDITOR_URL="http://download.cksource.com/CKEditor/CKEditor/CKEditor%204.5.6/ckeditor_4.5.6_standard.tar.gz"
    #curl -fSL $CKEDITOR_URL -o ckeditor.tar.gz \
    mv /ckeditor.tar.gz /var/www/guifi/sites/all/modules/ckeditor/ckeditor \
        && tar -xz --strip-components=1 -f ckeditor.tar.gz \
        && rm ckeditor.tar.gz \
        && chown -R www-data:www-data .

    # guifi dev db
    gunzip /guifi66_devel.sql.gz
    drush sql-dump guifi66_devel.sql.gz

    # guifi modules

    git clone --quiet https://github.com/guifi/drupal-guifi /var/www/guifi/sites/all/modules/guifi
    #git --git-dir=/var/www/guifi/sites/all/modules/guifi/.git reset --hard 304920ba9b6eb1c89e0140c960b0a566284d98f1
    git clone --quiet https://github.com/guifi/drupal-budgets /var/www/guifi/sites/all/modules/budgets
    #git --git-dir=/var/www/guifi/sites/all/modules/budgets/.git reset --hard edb249025da17060e37efccdc0b5d65d88cb6eb6
    drush en -y budgets
    drush en -y guifi

    # budgets guifi
    # guifi.net
    # guifi.net 2011 theme

    mkdir /var/www/guifi/files
    mv /images /var/www/guifi/files/

    # guifi theme
    mkdir /var/www/guifi/sites/all/themes
    git clone --quiet https://github.com/guifi/drupal-theme_guifinet2011 /var/www/guifi/sites/all/themes/guifi.net2011
    drush en -y guifi.net2011
    drush vset theme_default guifi.net2011
    
fi

source /etc/apache2/envvars && exec /usr/sbin/apache2 -DFOREGROUND
# dummy exec
#exec ping -i 100 localhost
