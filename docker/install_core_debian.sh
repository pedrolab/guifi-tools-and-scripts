#!/bin/bash
# core packages for each alquimia machine

# stop script if encounters an error
set -e

if [ $(id -u) -ne 0 ]; then
    echo "run command as sudo or root"
    exit 2
fi

# debian jessie
# src https://docs.docker.com/engine/installation/debian/
APT_STR="deb https://apt.dockerproject.org/repo debian-jessie main"

# install docker

apt-key adv --keyserver hkp://pgp.mit.edu:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D
apt-get update || true
apt-get install -y apt-transport-https
echo $APT_STR > /etc/apt/sources.list.d/docker.list
apt-get update || true
apt-get install -y docker-engine

# install docker compose and other utilities
apt-get install -y tmux
pip install -U --user pip
pip install --user -U docker-compose

# COMMON_USER will not require to use root or sudo commands for docker
# I prefer to do this manually
#COMMON_USER=$(ls /home)
#usermod -aG docker $COMMON_USER