Important note: This is a dirty container installation. DO NOT USE LIKE THIS IN A PRODUCTION ENVIRONMENT

# Requirements
- Install latest version of docker engine and docker compose. Debian or Ubuntu is recommended, but docker run also in Windows and Mac.
- Enough space, size of images: 1.062 GB + 302.1 MB
- Your ports 8080 and 8081 are available. If not, change appropiately in `docker-compose.yml` file


# Getting started

Go to a shell, enter this directory and execute: `docker-compose up`. Wait until the images are prepared, after that, all the things should be mounting in localhost.

To check web APP running, enter with a web browser http://localhost:8080/sentilo-catalog-web

To check API running, use with a shell `curl localhost:8081`.

Default user/password: admin/1234
