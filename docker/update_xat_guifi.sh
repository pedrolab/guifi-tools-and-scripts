#!/bin/bash

# upgrade rocketchat part

docker rm -fv rocketchat_old
docker pull rocket.chat
docker rename rocketchat rocketchat_old
docker stop rocketchat_old
docker run --restart=always --name rocketchat -p 3000:3000 --env ROOT_URL=https://xat.guifi.net --link db -d rocket.chat
