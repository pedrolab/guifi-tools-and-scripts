#!/bin/bash

# DESCRIPTION Script that facilitates flashing ubiquiti devices for qMp or other firmwares, specially in workshops
# MAINTAINER guifipedro 2015
# VERSION 1.0.0
# LICENSE http://www.gnu.org/licenses/gpl-3.0.en.html 

# Before running this script make sure your device:
# - is on reset mode (check from minute 0 to 1 this link https://help.ubnt.com/hc/en-us/articles/204911324-airMAX-How-to-reset-your-device-with-TFTP-firmware-recovery)
# - is connected to your computer (no network config required)

# variables
IP=192.168.1.20
NETMASK=/24
IFACE=eth0
FILE=qmp.bin

# appropriate print for GUI or CLI
# this script assumes that dumb TERM means GUI
print_info() {
    if [ "$TERM" = "dumb" ]; then
        zenity --info --text="$1"
    else
        printf '%s\n' "[INFO] ""$1"
    fi
}
print_err() {
    if [ "$TERM" = "dumb" ]; then
        zenity --error --text="$1"
    else
        printf '%s\n' "[ERROR] ""$1"
    fi
}

configure_network() {
    local conf_net_info=$"Configure network for flashing assuming you have sudo with your current user:
    "
    local conf_net_cmd=$"ip addr add $IP$NETMASK dev $IFACE"
    if [ "$TERM" = "dumb" ]; then
        if ! command -v gksu; then
            local no_gksu_err=$"gksu command not found, please install it:
  sudo apt-get install gksu
gksu is required to configure network.
Alternatively you can configure your network manually"
            zenity --error --text="$no_gksu_err"
            exit 1
        else
            local sudo=$"gksu "
            $sudo$conf_net_cmd -D "$conf_net_info$sudo$conf_net_cmd"
        fi
    else
        local sudo=$"sudo "
        printf '%s\n' "$conf_net_info$sudo$conf_net_cmd"
        $sudo$conf_net_cmd
    fi
}

processing_tftp() {
    local info_proc=$"Processing tftp command... (timeout 25 s)"
    # src http://ubuntuforums.org/showthread.php?t=2172828
    if [ "$TERM" = "dumb" ]; then
        # src http://askubuntu.com/questions/385003/is-zenity-progress-bar-affecting-variables 
        # src http://askubuntu.com/questions/316280/zenity-progress-bar-and-output 
        (
        tftp $IP -m binary -v -c put $FILE > temp
        printf '%s\n' "# ""Finished"
        ) | zenity --progress --pulsate --no-cancel --text="$info_proc"

        local tftp_log=$(<temp)
        rm -rf temp
    else
        printf '%s\n' "$info_proc"
        local tftp_log=`tftp $IP -m binary -v -c put $FILE`
    fi
}

# check zenity installation only if we are using GUI version
if ! command -v zenity &> /dev/null && [ "$TERM" = "dumb" ]; then
    no_zenity_err=$"Zenity command not found, please install it:
  sudo apt-get install zenity
Zenity is required to display graphical dialogs.
You can run this application without zenity through command line."
    printf '%s\n' "[ERROR] "$"<`date`>"" $no_zenity_err" > qmp_error_zenity_not_found_read_for_details
    exit 1
# fi
fi 

# src http://stackoverflow.com/questions/1298066/check-if-a-package-is-installed-and-then-install-it-if-its-not 
# check tftp installation
if [ ! command -v tftp &> /dev/null ]; then
  no_tftp_err=$"tftp command not found, please install it: sudo apt-get install tftp-hpa"
  print_err "$no_tftp_err"
  exit 1
fi

if [ ! -e $FILE ]; then
  # src http://stackoverflow.com/questions/3005963/how-can-i-have-a-newline-in-a-string-in-sh
  no_file_err=$"$FILE not found in this directory:
  1. Download appropriate image for your device from fw.qmp.cat
  2. Put it in the same directory as this script
  3. Copy or rename it to $FILE"
  print_err "$no_file_err"
  exit 2
fi

# if IP is not defined in routing try to configure it
if ! ip r | grep --quiet $IP; then
    configure_network
fi

#src http://stackoverflow.com/questions/18123211/checking-host-availability-by-using-ping-in-bash-scripts
if ping -w1 -c1 $IP &> /dev/null; then
    processing_tftp
    print_info $"tftp log:
""$tftp_log"$"

If succeeded wait 5 minutes until the
device loads the new firmware."

else
    host_down_err=$"Host IP ($IP) not responding.
Try again with the flash process from the beginning."
    print_err "$host_down_err"
    exit 3
fi
