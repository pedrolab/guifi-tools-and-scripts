#!/bin/sh

# DESCRIPTION
#  /guifinization/ of non Guifi.net infrastructures.
#  Generates GRE tunnel RFC2784 part of a local node (remote is configured the same way).
#
#  crontab -e    
#   * * * * * /path/to/dynGRE.sh   
#   * * * * * (sleep 30; /path/to/dynGRE.sh)

# MAINTAINER guifipedro 2015
# VERSION 1.0.0
# LICENSE http://www.gnu.org/licenses/gpl-3.0.en.html

# General variables
#ping deadline (minimum 1, default 10)
PW=10
# count config files
COUNT_CONF=0
# [local node] listen all interfaces
LO_PUB=0.0.0.0
# location of script
SCRIPT=$(readlink -f "$0")
SCRIPTPATH=$(dirname "$SCRIPT")

# get_ip returns an IP from an URL (dynamic IP)
get_ip() {
    # in Debian
    if command -v host &> /dev/null; then
        local output=$(host "$@" | grep "has address" | tail -1 | awk '{print $4}')
    # in qMp (OpenWRT-based)
    else
        local output=$(nslookup "$@" | tail -1 | awk '{print $3}' ) 2> /dev/null
    fi
    # if there is an error during resolution start program exiting
    if [ -z $output ]; then
        logger -s -t $LOG_NAME "$RE_DYN is not resolving correctly"
        return 1
    fi

    echo $output
}

# GRE commands to configure it
config_gre() {
    ip tunnel add $TUN_IFACE mode gre remote $RE_PUB local $LO_PUB ttl 255
    ip link set $TUN_IFACE up
    ip -4 addr add $LO_TUN dev $TUN_IFACE
}

if [ $(id -u) -ne 0 ]; then
    logger -s -t "dynGRE" "this script requires root permissions"
    exit 2
fi

for config in $SCRIPTPATH/*.conf; do
    source $config

    # if set is not enabled, 0 by default, if is not 1 exit this config file
    enable=${enable:-0}
    if ! [ $enable -eq 1 ]; then
        continue
    fi

    # log name for /var/log/syslog or logread
    LOG_NAME="dynGRE $TUN_IFACE"
    
    RE_PUB=$(ip tunnel show $TUN_IFACE | awk '{print $4}')
    
    # if there is no Remote Public IP (RE_PUB), gre is not configured
    if [ -z $RE_PUB ]; then
        # delete potential bad configured gre
        ip link delete $TUN_IFACE &> /dev/null
        RE_PUB=$(get_ip $RE_DYN) || exit 1
        config_gre
        logger -s -t $LOG_NAME "start $TUN_IFACE"
    fi
    
    # if there is no ping response from the running remote tunnel ip,
    # probably gre requires to be reconfigured
    # this ping also helps to remain nat tables open
    if ! ping -w$PW -c1 $RE_TUN &> /dev/null; then
        logger -s -t $LOG_NAME "Remote Tunnel IP $RE_TUN not responding"
        NEW_RE_PUB=$(get_ip $RE_DYN) || exit 1
        # remote ip changed, gre requires to be reconfigured
        if [ "$RE_PUB" != "$NEW_RE_PUB" ]; then
            RE_PUB=$NEW_RE_PUB
            logger -s -t $LOG_NAME "$RE_PUB -> $NEW_RE_PUB"
            # delete previous link and tunnel
            ip link delete $TUN_IFACE
            config_gre
        fi
    fi

    let COUNT_CONF=COUNT_CONF+1

done

if [ $COUNT_CONF -eq 0 ]; then
    logger -s -t dynGRE "cannot read any tunnel config file. Exist? Have enable=1?" 
fi
