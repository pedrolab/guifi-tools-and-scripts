# Installing obs

Debian sometimes work, use `install-obs.sh`

Ubuntu, use this info from this [source](https://obsproject.com/download)
```
14.04: FFmpeg is required.
If you do not have FFmpeg installed (if you're not sure, then you probably don't have it), you can get it with the following commands: 

sudo add-apt-repository ppa:kirillshkrogalev/ffmpeg-next
sudo apt-get update && sudo apt-get install ffmpeg 

14.04/15.04+: You can install OBS with the following commands: 

sudo add-apt-repository ppa:obsproject/obs-studio
sudo apt-get update && sudo apt-get install obs-studio
```

# Screen capture of speaker

Mix speaker's screen with video camera source and do a standard streaming.

Mixer software: [Open Broadcaster Software](https://obsproject.com/)

## MJPEG

Obs configuration: add source `Media Source` unset `local file`, input url (http:// or udp://) and input format mjpeg.

Pros
- Low latency
- Low cpu usage

Cons
- High bandwidth usage. For a semistatic content, requires 20 Mbps.

### VLC client (http - passive)

Debian 8 Jessie with by default VLC `apt-get install vlc`: `cvlc -vvv --no-audio screen:// --screen-fps 4 --sout "#transcode{vcodec=MJPG,vb=15000,scale=1}:standard{access=http,mux=mpjpeg,dst=:18223/}"`

A more cleaner way (not tested): `cvlc -vvv screen:// --sout "#transcode{vcodec=MJPG,vb=15000,scale=1,acodec=none,fps=4}:standard{access=http,mux=mpjpeg,dst=:18223/}"`

Windows 7 with VLC 2.1.4 Ricewind
- disable firewall or make an exception for VLC
- `cd "Program Files\VideoLAN\VLC"`
- `vlc.exe --no-audio screen:// --screen-fps 4 --sout "#transcode{vcodec=MJPG,vb=15000,scale=1}:standard{access=http,mux=mpjpeg,dst=:18223/}"`

MacOS (not tested)

- Known issues: I do not know how to make the mouse pointer appear

### avconv client (rtp - active)

Debian 8 Jessie stable with just `apt-get install libav-tools`: `avconv -f x11grab -r 10 -s 1024x768 -i :0.0 -f mjpeg -q 1 udp://${obs_url}:${random_port}`
