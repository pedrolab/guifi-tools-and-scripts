#!/bin/bash

# if for example script stopped at line 12, 
# continue from line 13 with this command: tail -n+12 install-obs.sh

set -v
set -e
set -u
# validate root
sudo -v

# cleanup (previous uses of this script)
# uncomment this line after first installation
# sudo apt-get purge -y obs-studio ffmpeg
rm -rf ~/ffmpeg
rm -rf ~/obs-studio

# start

# preliminaries
sudo apt-get install -y build-essential pkg-config cmake git checkinstall

sudo apt-get install -y libx11-dev libgl1-mesa-dev libpulse-dev libxcomposite-dev \
        libxinerama-dev libv4l-dev libudev-dev libfreetype6-dev \
        libfontconfig-dev qtbase5-dev libqt5x11extras5-dev libx264-dev \
        libxcb-xinerama0-dev libxcb-shm0-dev libjack-jackd2-dev libcurl4-openssl-dev

sudo apt-get install -y libvpx-dev libvorbis-dev

sudo apt-get install -y zlib1g-dev yasm

cd ~

# compile & install ffmpeg
git clone --depth 1 git://source.ffmpeg.org/ffmpeg.git
cd ffmpeg

./configure --enable-gpl --enable-shared --enable-libvpx --enable-libvorbis --enable-libx264 --prefix=/usr

make -j4

sudo checkinstall --pkgname=FFmpeg --fstrans=no --backup=no --pkgversion="$(date +%Y%m%d)-git" --deldoc=yes -y

sudo dpkg -i *.deb
cd ..

# compile & install obs
git clone https://github.com/jp9000/obs-studio.git
cd obs-studio
mkdir build && cd build
cmake -DUNIX_STRUCTURE=1 -DCMAKE_INSTALL_PREFIX=/usr ..
make -j4

sudo checkinstall --pkgname=obs-studio --fstrans=no --backup=no --pkgversion="$(date +%Y%m%d)-git" --deldoc=yes -y

sudo dpkg -i *.deb
